package io.gitlab.danielrparks.vibrato;


import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.media.AudioManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import ca.antonious.materialdaypicker.MaterialDayPicker;

public class ScheduleCardAdapter extends RecyclerView.Adapter<ScheduleCardAdapter.ScheduleCardViewHolder> {
    static final int[] LIST_TO_RINGER_MODE;
    static final int[] RINGER_MODE_TO_LIST;
    static {
        LIST_TO_RINGER_MODE = new int[]{AudioManager.RINGER_MODE_VIBRATE, AudioManager.RINGER_MODE_NORMAL, AudioManager.RINGER_MODE_SILENT};
        RINGER_MODE_TO_LIST = new int[AudioManager.RINGER_MODE_NORMAL + 1]; // hopefully this will work with weird non-standard versions of Android
        //noinspection ConstantConditions
        RINGER_MODE_TO_LIST[AudioManager.RINGER_MODE_VIBRATE] = 0;
        RINGER_MODE_TO_LIST[AudioManager.RINGER_MODE_NORMAL] = 1;
        RINGER_MODE_TO_LIST[AudioManager.RINGER_MODE_SILENT] = 2;
    }
    @SuppressWarnings("FieldCanBeLocal")
    // local would be super messy
    class ScheduleCardViewHolder extends RecyclerView.ViewHolder {
        private FragmentManager fm;
        private int index;

        String calendarToDateString(@NonNull Calendar c) {
            // this is NOT IDEAL. I would like to print localized date/time only, but Android doesn't support java.time until Oreo.
            // if you know of a better way to do this, please file an issue or a pull request!
            StringBuilder result = new StringBuilder();
            result.append(c.get(Calendar.DAY_OF_MONTH));
            result.append(' ');
            if (! backingSchedule.repeat) {
                result.append(c.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.getDefault()));
                result.append(' ');
                result.append(c.get(Calendar.YEAR));
            }
            else {
                result.append(c.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()));
            }
            return result.toString();
        }

        String calendarToTimeString(@NonNull Calendar c) {
            // same here, please file issue/PR
            StringBuilder result = new StringBuilder();
            result.append(c.get(Calendar.HOUR_OF_DAY));
            result.append(":");
            String n = String.valueOf(c.get(Calendar.MINUTE));
            if (n.length() < 2) result.append('0');
            result.append(n);
            return result.toString();
        }

        private Schedule backingSchedule;
        private EditText timeBox, dateBox1, dateBox2;
        private Spinner ringerBox;
        private CheckBox repeatBox;
        private MaterialDayPicker dayBox;

        ScheduleCardViewHolder(CardView cv, FragmentManager fm) {
            super(cv);
            this.fm = fm;
            index = -1;
            timeBox = cv.findViewById(R.id.timebox);
            timeBox.setOnClickListener(timeClickListener);
            dateBox1 = cv.findViewById(R.id.datebox1);
            dateBox1.setOnClickListener(new dateClickListener(beginDateSetListener, 1));
            dateBox2 = cv.findViewById(R.id.datebox2);
            dateBox2.setOnClickListener(new dateClickListener(endDateSetListener, 2));
            repeatBox = cv.findViewById(R.id.repeatbox);
            repeatBox.setOnCheckedChangeListener(repeatSetListener);
            dayBox = cv.findViewById(R.id.daybox);
            dayBox.setDaySelectionChangedListener(daySetListener);
            ringerBox = cv.findViewById(R.id.boxringer);
            ringerBox.setOnItemSelectedListener(modeSetListener);
            FloatingActionButton deleteButton = cv.findViewById(R.id.delete_button);
            deleteButton.setOnClickListener(deleteListener);
        }

        void update(Schedule s, int index) {
            this.backingSchedule = s;
            this.index = index;
            update();
        }

        private void setExpectedValues() {
            modeSetListener.expectedValue = RINGER_MODE_TO_LIST[backingSchedule.ringerMode];
            repeatSetListener.expectedValue = backingSchedule.repeat;
            daySetListener.expectedValue = backingSchedule.daysOfWeek.getWeekdayList();
        }

        void update() {
            synchronized (MainActivity.schedLock) {
                setExpectedValues();
                ringerBox.setSelection(RINGER_MODE_TO_LIST[backingSchedule.ringerMode]);
                timeBox.setText(calendarToTimeString(backingSchedule.time));
                dateBox1.setText(calendarToDateString(backingSchedule.begin));
                dateBox2.setText(calendarToDateString(backingSchedule.end));
                repeatBox.setChecked(backingSchedule.repeat);
                dayBox.setSelectedDays(backingSchedule.daysOfWeek.getWeekdayList());
            }
        }

        private class ExpectedModeSetListener implements AdapterView.OnItemSelectedListener {
            private int expectedValue = -1;
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if (position != expectedValue) {
                    expectedValue = position;
                    synchronized (MainActivity.schedLock) {
                        backingSchedule.ringerMode = LIST_TO_RINGER_MODE[position];
                    }
                    suh.updatedSchedule(false);
                    // on views that show the result of user interaction (i.e. no custom UI stuff), we don't have to reload from the backing schedule
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // do nothing, duh
            }
        }
        private ExpectedModeSetListener modeSetListener = new ExpectedModeSetListener();

        private TimePickerDialog.OnTimeSetListener timeSetListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hour, int minute) {
                synchronized (MainActivity.schedLock) {
                    backingSchedule.time.set(Calendar.HOUR_OF_DAY, hour);
                    backingSchedule.time.set(Calendar.MINUTE, minute);
                }
                suh.updatedSchedule(false);
                update();
            }
        };

        private DatePickerDialog.OnDateSetListener beginDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                synchronized (MainActivity.schedLock) {
                    backingSchedule.begin.set(year, month, day);
                }
                suh.updatedSchedule(false);
                update();
            }
        };

        private DatePickerDialog.OnDateSetListener endDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                synchronized (MainActivity.schedLock) {
                    backingSchedule.end.set(year, month, day);
                }
                suh.updatedSchedule(false);
                update();
            }
        };

        private View.OnClickListener timeClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment tp = new TimePickerFragment(timeSetListener, backingSchedule.time);
                tp.show(fm, "timePicker");
            }
        };

        private class dateClickListener implements View.OnClickListener {
            private DatePickerDialog.OnDateSetListener setListener;
            private int datebox;
            dateClickListener(DatePickerDialog.OnDateSetListener setListener, int datebox) {
                this.setListener = setListener;
                this.datebox = datebox;
            }
            @Override
            public void onClick(View view) {
                Calendar parentDate;
                if (datebox == 1) parentDate = backingSchedule.begin;
                else parentDate = backingSchedule.end;
                DialogFragment dp = new DatePickerFragment(setListener, parentDate);
                dp.show(fm, "datePicker");
            }
        }

        private class ExpectedRepeatSetListener implements CompoundButton.OnCheckedChangeListener {
            private boolean expectedValue = true;
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b != expectedValue) {
                    expectedValue = b;
                    synchronized (MainActivity.schedLock) {
                        backingSchedule.repeat = b;
                    }
                    suh.updatedSchedule(false);
                    update(); // have to update here because date format will change
                }
            }
        }
        private ExpectedRepeatSetListener repeatSetListener = new ExpectedRepeatSetListener();

        private class ExpectedDaySetListener implements MaterialDayPicker.DaySelectionChangedListener {
            private List<MaterialDayPicker.Weekday> expectedValue = new ArrayList<>();
            @Override
            public void onDaySelectionChanged(@NonNull List<MaterialDayPicker.Weekday> list) {
                if (! list.equals(expectedValue)) {
                    expectedValue = list;
                    synchronized (MainActivity.schedLock) {
                        backingSchedule.daysOfWeek.updateFromWeekdayList(list);
                    }
                    suh.updatedSchedule(false);
                }
            }
        }
        private ExpectedDaySetListener daySetListener = new ExpectedDaySetListener();

        private FloatingActionButton.OnClickListener deleteListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               deleteIndex(index);
            }
        };
    }

    private ArrayList<Schedule> schedules;
    private FragmentManager fm;
    private MainActivity.ScheduleUpdateHandler suh;

    ScheduleCardAdapter(ArrayList<Schedule> schedules, FragmentManager fm, MainActivity.ScheduleUpdateHandler suh) {
        this.schedules = schedules;
        this.fm = fm;
        this.suh = suh;
    }

    @NonNull
    @Override
    public ScheduleCardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CardView v = (CardView) LayoutInflater.from(parent.getContext()).inflate(R.layout.schedule_card, parent, false);
        return new ScheduleCardViewHolder(v, fm);
    }

    public void onBindViewHolder(@NonNull ScheduleCardViewHolder scvh, int pos) {
        scvh.update(schedules.get(pos), pos);
    }

    public int getItemCount() {
        return schedules.size();
    }

    private void deleteIndex(int index) {
        synchronized (MainActivity.schedLock) {
            schedules.remove(index);
        }
        suh.updatedSchedule(true);
        notifyItemRemoved(index);
        notifyDataSetChanged();
    }
}
