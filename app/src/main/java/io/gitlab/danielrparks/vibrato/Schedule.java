package io.gitlab.danielrparks.vibrato;

import androidx.annotation.NonNull;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class Schedule implements Serializable {
    int ringerMode;
    Calendar time;
    Calendar begin;
    Calendar end;
    boolean repeat;
    WeekdaySelectionChecker daysOfWeek;
    private static final long serialVersionUID = 1L;

    // needed for testing
    @SuppressWarnings("unused")
    Schedule(int ringerMode, Calendar time, Calendar begin, Calendar end, boolean repeat, WeekdaySelectionChecker daysOfWeek) {
        this.ringerMode = ringerMode;
        this.time = time;
        this.begin = begin;
        this.end = end;
        this.repeat = repeat;
        this.daysOfWeek = daysOfWeek;
    }

    Schedule() {
        // I have to use ugly code here because the good stuff wasn't added until Oreo
        ringerMode = ScheduleCardAdapter.LIST_TO_RINGER_MODE[(int) (Math.random() * 3)]; // lol
        time = Calendar.getInstance();
        time.set(Calendar.SECOND, 0);
        time.set(Calendar.MILLISECOND, 0);
        time.add(Calendar.HOUR, 2); // don't make them trigger it right away by accident, that would be really annoying!
        begin = Calendar.getInstance();
        begin.set(Calendar.MONTH, (Calendar.JANUARY + begin.get(Calendar.MONTH))/2);
        zeroTimeFields(begin);
        end = Calendar.getInstance();
        end.set(Calendar.MONTH, (Calendar.DECEMBER + end.get(Calendar.MONTH))/2); // also lol, see if you can figure out why I did that
        zeroTimeFields(end);
        repeat = true;
        daysOfWeek = new WeekdaySelectionChecker(new boolean[]{true, true, true, true, true, true, true});
    }

    private void setTimeFields(@NonNull Calendar c) {
        c.set(Calendar.HOUR_OF_DAY, time.get(Calendar.HOUR_OF_DAY));
        c.set(Calendar.MINUTE, time.get(Calendar.MINUTE));
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
    }

    private static void zeroTimeFields(@NonNull Calendar c) {
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
    }

    long calculateNext(Calendar c) {
        if (! daysOfWeek.hasSelection()) return -1;
        Calendar now = (Calendar) c.clone();
        if (!repeat) {
            if (now.compareTo(end) > 0) return -1;
            if (now.compareTo(begin) < 0) { // we haven't started yet, so fast forward to start
                now = (Calendar) begin.clone(); // only cloning because I don't want to accidentally modify begin
                now.add(Calendar.DATE, -1); // decrement so that tomorrow is begin
            }
            now.add(Calendar.DATE, daysOfWeek.getNextDay(now)); // the next allowed day of the week to trigger
            if (now.compareTo(end) <= 0) { // still within the bounds, so that's the value we're after!
                setTimeFields(now);
                return now.getTimeInMillis();
            }
            else return -1; // not possible, don't set the alarm
        }
        else {
            while (true) { // loop until we find a date with a suitable day of week
                begin.set(Calendar.YEAR, now.get(Calendar.YEAR));
                end.set(Calendar.YEAR, now.get(Calendar.YEAR)); // avoid getting broken by leap years lol
                // this loop must finish, because every repeating date range is valid, and every date is each day of the week in some year
                if (begin.get(Calendar.DAY_OF_YEAR) > end.get(Calendar.DAY_OF_YEAR)) {
                    // something like December 1 - February 1
                    if (now.get(Calendar.DAY_OF_YEAR) < begin.get(Calendar.DAY_OF_YEAR) && now.get(Calendar.DAY_OF_YEAR) > end.get(Calendar.DAY_OF_YEAR)) {
                        // we're outside the range, fast-forward
                        now.set(Calendar.DAY_OF_YEAR, begin.get(Calendar.DAY_OF_YEAR) - 1);
                    }
                    now.add(Calendar.DATE, daysOfWeek.getNextDay(now));
                    if (now.get(Calendar.DAY_OF_YEAR) >= begin.get(Calendar.DAY_OF_YEAR) || now.get(Calendar.DAY_OF_YEAR) <= end.get(Calendar.DAY_OF_YEAR)) {
                        // valid!
                        setTimeFields(now);
                        return now.getTimeInMillis();
                    }
                    else {
                        // fast-forward to next beginning and try again
                        now.set(Calendar.DAY_OF_YEAR, begin.get(Calendar.DAY_OF_YEAR) - 1);
                    }
                }
                else {
                    // more normal range, less brain-boggling
                    if (now.get(Calendar.DAY_OF_YEAR) < begin.get(Calendar.DAY_OF_YEAR)) {
                        // outside range, fast-forward
                        // now = (Calendar) begin.clone();
                        // now.add(Calendar.DATE, -1);
                        now.set(Calendar.DAY_OF_YEAR, begin.get(Calendar.DAY_OF_YEAR) - 1);
                    }
                    else if (now.get(Calendar.DAY_OF_YEAR) > end.get(Calendar.DAY_OF_YEAR)) {
                        // we still need to fast forward
                        // but this time into NEXT YEAR
                        now.set(Calendar.DAY_OF_YEAR, begin.get(Calendar.DAY_OF_YEAR));
                        now.add(Calendar.YEAR, 1);
                    }
                    now.add(Calendar.DATE, daysOfWeek.getNextDay(now));
                    if (now.get(Calendar.DAY_OF_YEAR) >= begin.get(Calendar.DAY_OF_YEAR) && now.get(Calendar.DAY_OF_YEAR) <= end.get(Calendar.DAY_OF_YEAR)) {
                        // valid!
                        setTimeFields(now);
                        return now.getTimeInMillis();
                    }
                    else {
                        // fast-forward to the beginning next year and try again
                        now.add(Calendar.YEAR, 1);
                        now.set(Calendar.DAY_OF_YEAR, begin.get(Calendar.DAY_OF_YEAR) - 1);
                    }
                }
                // oh no, we didn't find a date this year!
            }
        }
    }

    private static DateFormat DATE_ONLY = new SimpleDateFormat("MM/dd/yyyy", Locale.US);

    @NonNull
    public String toString() {
        return "Schedule: " +
                ringerMode +
                " at " +
                time.get(Calendar.HOUR_OF_DAY) +
                ":" +
                time.get(Calendar.MINUTE) +
                " from " +
                DATE_ONLY.format(begin.getTime()) +
                " to " +
                DATE_ONLY.format(end.getTime()) +
                (repeat ? " repeating, " : " not repeating, ") +
                daysOfWeek;
    }
}
