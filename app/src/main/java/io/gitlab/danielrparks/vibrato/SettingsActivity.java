package io.gitlab.danielrparks.vibrato;

import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.EditTextPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        SettingsFragment f = new SettingsFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.settings, f)
                .commit();
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    public static class SettingsFragment extends PreferenceFragmentCompat {
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey);
            EditTextPreference ep = findPreference("timingbuffer");
            //noinspection ConstantConditions
            ep.setOnPreferenceChangeListener(numberCheckListener);
            ep.setDialogLayoutResource(R.layout.preference_dialog_edittext_number);
        }
        private Preference.OnPreferenceChangeListener numberCheckListener = new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                String result = (String) newValue;
                try {
                    Double.parseDouble(result);
                    return true;
                } catch (NumberFormatException e) {
                    Toast.makeText(getContext(), R.string.not_number, Toast.LENGTH_LONG).show();
                    return false;
                }
            }
        };
    }

}