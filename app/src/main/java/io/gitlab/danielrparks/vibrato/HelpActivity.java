package io.gitlab.danielrparks.vibrato;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class HelpActivity extends AppCompatActivity {

    @SuppressLint("SetTextI18n") // obviously the build info won't be translated
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        ((TextView) findViewById(R.id.build_number_actual)).setText(BuildConfig.VERSION_NAME + " " + BuildConfig.BUILD_TYPE);
        findViewById(R.id.faq).setOnClickListener(new urlClickListener("https://danielrparks.gitlab.io/vibrato/faq"));
        findViewById(R.id.website).setOnClickListener(new urlClickListener("https://danielrparks.gitlab.io/vibrato"));
        findViewById(R.id.source).setOnClickListener(new urlClickListener("https://gitlab.com/danielrparks/vibrato"));
        findViewById(R.id.lib_materialdaypicker).setOnClickListener(new urlClickListener("https://github.com/gantonious/MaterialDayPicker"));
        findViewById(R.id.bugtracker).setOnClickListener(new urlClickListener("https://gitlab.com/danielrparks/vibrato/issues"));
    }

    private class urlClickListener implements View.OnClickListener {
        private String url;

        urlClickListener(String url) {
            this.url = url;
        }

        @Override
        public void onClick(View view) {
            Intent openBrowser = new Intent();
            openBrowser.setAction(Intent.ACTION_VIEW);
            openBrowser.setData(Uri.parse(url));
            if (openBrowser.resolveActivity(getPackageManager()) != null) {
                startActivity(openBrowser);
            }
        }
    }
}
