package io.gitlab.danielrparks.vibrato;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    @SuppressWarnings("FieldCanBeLocal") // for style
    private RecyclerView scheduleList;
    private RecyclerView.Adapter sLAdapter;
    @SuppressWarnings("FieldCanBeLocal")
    private RecyclerView.LayoutManager sLLayoutManager;
    public static ArrayList<Schedule> schedules;
    public static final String TAG = "MainActivity";
    public static final String schedDataFilename = "schedules.ser";
    public static File schedData = null;
    public static FileOutputStream schedDataOut = null;
    public static final Object schedLock = new Object();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        synchronized (schedLock) {
            schedData = new File(getApplicationContext().getFilesDir(), schedDataFilename);
            // yes, I'm loading files on the UI thread
            // this data must be available as soon as the user opens the app
            try {
                ObjectInputStream schedDataIn = new ObjectInputStream(new FileInputStream(schedData));
                //noinspection unchecked
                schedules = (ArrayList<Schedule>) schedDataIn.readObject();
                schedDataIn.close();
            } catch (FileNotFoundException e) {
                schedules = new ArrayList<>();
            } catch (IOException e) {
                Log.e(TAG, "could not read schedule data, overwriting!", e);
                schedules = new ArrayList<>();
            } catch (ClassCastException | ClassNotFoundException e) {
                // stop messing with my files
                // if serialVersionUID changes, I will update this code to migrate the data
                Log.e(TAG, "schedule data is corrupt, overwriting!");
                schedules = new ArrayList<>();
            }
        }

        FloatingActionButton fab = findViewById(R.id.fab);
        final ScheduleUpdateHandler suh = new ScheduleUpdateHandler();
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                synchronized (schedLock) {
                    schedules.add(0, new Schedule());
                }
                suh.updatedSchedule(false);
                sLAdapter.notifyDataSetChanged();
            }
        });
        scheduleList = findViewById(R.id.schedules);
        scheduleList.setHasFixedSize(true);
        scheduleList.addItemDecoration(new VerticalSpacingItemDecoration(8));
        sLLayoutManager = new LinearLayoutManager(this);
        scheduleList.setLayoutManager(sLLayoutManager);
        sLAdapter = new ScheduleCardAdapter(schedules, getSupportFragmentManager(), new ScheduleUpdateHandler());
        scheduleList.setAdapter(sLAdapter);
        FragmentManager fm = getSupportFragmentManager();
        if (Build.VERSION.SDK_INT >= 23) {
            NotificationManager nm = (NotificationManager) getApplicationContext().getSystemService(NOTIFICATION_SERVICE);
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            if (nm != null) {
                if (! nm.isNotificationPolicyAccessGranted() && ! sp.getBoolean("dnd_requested", false)) {
                    Intent permIntent = new Intent();
                    permIntent.setAction(Settings.ACTION_NOTIFICATION_POLICY_ACCESS_SETTINGS);
                    DialogFragment notifDialog = new PermissionDialogFragment(R.string.needs_dnd_access, R.string.dnd_backup_instructions, "dnd_requested", permIntent);
                    notifDialog.show(fm, "dndAccessDialog");
                }
                if (Build.VERSION.SDK_INT >= 26) {
                    NotificationChannel channel = new NotificationChannel(SetRingerModeReceiver.CHANNEL_ID, getString(R.string.notif_channel_name), NotificationManager.IMPORTANCE_DEFAULT);
                    nm.createNotificationChannel(channel);
                }
            }
            PowerManager pm = (PowerManager) getApplicationContext().getSystemService(POWER_SERVICE);
            if (pm != null) {
                if (! pm.isIgnoringBatteryOptimizations(getPackageName()) && ! sp.getBoolean("whitelist_requested", false)) {
                    Intent permIntent = new Intent();
                    /*
                    Here's why this is necessary:
                    This app is designed to be used only when the user needs to change their schedules.
                    Otherwise, the app quietly and automatically executes their schedules.
                    This means that the app will likely end up in the Rare app standby bucket, meaning that its alarms could be deferred by up to 2 hours.
                    This, obviously, would adversely affect the core functionality of the app.
                     */
                    permIntent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                    permIntent.setData(Uri.parse("package:" + getPackageName()));
                    DialogFragment notifDialog = new PermissionDialogFragment(R.string.needs_battery_whitelist, R.string.whitelist_backup_instructions, "whitelist_requested", permIntent);
                    notifDialog.show(fm, "batteryWhitelistDialog");
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        try {
            schedDataOut.close();
        } catch (IOException | NullPointerException e) {
            Log.w(TAG, "failed to close output file", e);
        }
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent startSettings = new Intent(this,  SettingsActivity.class);
            startActivity(startSettings);
            return true;
        }
        else if (id == R.id.action_help) {
            Intent startHelp = new Intent(this, HelpActivity.class);
            startActivity(startHelp);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class ScheduleUpdateHandler {
        void updatedSchedule(boolean deleteLast) {
            //noinspection unchecked
            new UpdateAlarmsTask(getApplicationContext(), deleteLast).execute(schedules);
        }
    }
}
