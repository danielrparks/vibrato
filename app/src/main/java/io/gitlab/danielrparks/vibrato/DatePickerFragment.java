package io.gitlab.danielrparks.vibrato;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import java.util.Calendar;
import java.util.Objects;

public class DatePickerFragment extends DialogFragment {
    private DatePickerDialog.OnDateSetListener setListener;
    private Calendar parentDate;

    DatePickerFragment(DatePickerDialog.OnDateSetListener setListener, Calendar parentDate) {
        this.setListener = setListener;
        this.parentDate = parentDate;
    }

    @NonNull
    public Dialog onCreateDialog(Bundle savedState) {
        return new DatePickerDialog(Objects.requireNonNull(getActivity()), setListener, parentDate.get(Calendar.YEAR), parentDate.get(Calendar.MONTH), parentDate.get(Calendar.DAY_OF_MONTH));
    }
}
