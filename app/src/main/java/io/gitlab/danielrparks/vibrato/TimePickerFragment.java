package io.gitlab.danielrparks.vibrato;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.format.DateFormat;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import java.util.Calendar;

public class TimePickerFragment extends DialogFragment {
    private TimePickerDialog.OnTimeSetListener setListener;
    private Calendar parentTime;

    TimePickerFragment(TimePickerDialog.OnTimeSetListener setListener, Calendar parentTime) {
        this.setListener = setListener;
        this.parentTime = parentTime;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedState) {
        return new TimePickerDialog(getActivity(), setListener, parentTime.get(Calendar.HOUR_OF_DAY), parentTime.get(Calendar.MINUTE), DateFormat.is24HourFormat(getActivity()));
    }
}
