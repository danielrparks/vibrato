package io.gitlab.danielrparks.vibrato;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.preference.PreferenceManager;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class SetRingerModeReceiver extends BroadcastReceiver {
    public static final String TAG = "SetRingerModeReceiver";
    public static final String CHANNEL_ID = "receiver_msgs";
    public static final int FAILED_ID = 5;
    public static final int SET_ID = 6;
    @Override
    public void onReceive(@NonNull Context context, Intent intent) {
        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        AlarmManager alm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        if (nm == null || am == null || alm == null) {
            Log.e(TAG, "Could not get system services!");
            return;
        }
        byte[] sb = intent.getByteArrayExtra("sched");
        int index = intent.getIntExtra("index", -1);
        if (sb == null || index == -1) {
            Log.e(TAG, "malformed intent");
            return;
        }
        Schedule s;
        ObjectInputStream ois;
        try {
            ois = new ObjectInputStream(new ByteArrayInputStream(sb));
            s = (Schedule) ois.readObject();
        } catch (IOException | ClassNotFoundException | ClassCastException e) {
            Log.e(TAG, "malformed intent", e);
            return;
        }
        if (Build.VERSION.SDK_INT >= 23 && ! nm.isNotificationPolicyAccessGranted() && (s.ringerMode == AudioManager.RINGER_MODE_SILENT || am.getRingerMode() == AudioManager.RINGER_MODE_SILENT)) {
            NotificationCompat.Builder nb = new NotificationCompat.Builder(context, CHANNEL_ID)
                    .setSmallIcon(R.drawable.notif_icon)
                    .setContentTitle(context.getString(R.string.notif_set_failed))
                    .setContentText(context.getString(R.string.notif_set_failed_desc))
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);
            nm.notify(FAILED_ID, nb.build());
            return;
        }
        am.setRingerMode(s.ringerMode); // lol this is the only important line in this entire class
        if (sp.getBoolean("toast", false)) {
            Toast.makeText(context, context.getString(R.string.notif_set) + " " + getRingerModeName(s.ringerMode, context), Toast.LENGTH_LONG).show();
        }
        if (sp.getBoolean("notification", false)) {
            NotificationCompat.Builder nb = new NotificationCompat.Builder(context, CHANNEL_ID)
                    .setSmallIcon(R.drawable.notif_icon)
                    .setContentTitle(context.getString(R.string.notif_set) + " " + getRingerModeName(s.ringerMode, context))
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);
            nm.notify(SET_ID, nb.build());
        }
        UpdateAlarmsTask uat = new UpdateAlarmsTask(context.getApplicationContext(), false);
        uat.scheduleAlarm(index, s, sb, context, alm, null);
    }

    private static String getRingerModeName(int ringerMode, @NonNull Context context) {
        return context.getResources().getStringArray(R.array.modes)[ScheduleCardAdapter.RINGER_MODE_TO_LIST[ringerMode]];
    }
}
