package io.gitlab.danielrparks.vibrato;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import androidx.preference.PreferenceManager;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.lang.ref.WeakReference;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Calendar;

public class UpdateAlarmsTask extends AsyncTask<ArrayList<Schedule>, Void, Void> {
    private static final String TAG = "UpdateAlarmsTask";
    private static final long ONE_MINUTE = 1000 * 60;


    private WeakReference<Context> mainActivityContext;
    private boolean deleteLast;
    private boolean exact;
    private long window;

    UpdateAlarmsTask(Context context, boolean deleteLast) {
        this.mainActivityContext = new WeakReference<>(context);
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        exact = sp.getBoolean("exact", false);
        double dwindow = 9;
        try {
            dwindow = Double.parseDouble(sp.getString("timingbuffer", "9"));
        } catch (NumberFormatException e) {
            Log.e(TAG, "couldn't get window from sharedPreferences, using 9");
        }
        window = (long) (dwindow * ONE_MINUTE);
        this.deleteLast = deleteLast;
    }

    @Override
    @SafeVarargs
    protected final Void doInBackground(ArrayList<Schedule>... arrayLists) {
        ArrayList<Schedule> schedules = arrayLists[0];
        synchronized (MainActivity.schedLock) {
            if (MainActivity.schedData == null) {return null;} // I guess we can't do anything...
            if (MainActivity.schedDataOut == null) {
                try {
                    MainActivity.schedDataOut = new FileOutputStream(MainActivity.schedData);
                } catch (FileNotFoundException e) {
                    Log.w(TAG, "could not open output file", e);
                    return null;
                }
            }
            FileOutputStream schedDataOut = MainActivity.schedDataOut;
            FileChannel fc = schedDataOut.getChannel();
            try {
                fc.position(0); //erase file
                fc.truncate(0);
                ObjectOutputStream oos = new ObjectOutputStream(schedDataOut);
                oos.writeObject(schedules);
            } catch (IOException e) {
                Log.w(TAG, "could not lock output file", e);
                return null;
            }
        }
        rescheduleAlarms(schedules);
        return null;
    }

    void rescheduleAlarms(ArrayList<Schedule> schedules) {
        Context c = mainActivityContext.get();
        AlarmManager am = (AlarmManager) c.getSystemService(Context.ALARM_SERVICE);
        if (am == null) {
            Log.e(TAG, "Could not schedule alarms!");
            return;
        }
        if (deleteLast) {
            PendingIntent pd = PendingIntent.getBroadcast(c, schedules.size(), new Intent(c, SetRingerModeReceiver.class), PendingIntent.FLAG_ONE_SHOT | PendingIntent.FLAG_UPDATE_CURRENT);
            am.cancel(pd);
            Log.d(TAG, "deleted alarm at index " + schedules.size());
        }
        synchronized (MainActivity.schedLock) {
            for (int i = 0; i < schedules.size(); i++) {
                Calendar now = Calendar.getInstance();
                Schedule si = schedules.get(i);
                if ((si.time.get(Calendar.HOUR_OF_DAY) == now.get(Calendar.HOUR_OF_DAY) && si.time.get(Calendar.MINUTE) > now.get(Calendar.MINUTE)) || si.time.get(Calendar.HOUR_OF_DAY) > now.get(Calendar.HOUR_OF_DAY)) {
                    now.add(Calendar.DATE, -1);
                    // schedule today's alarm if it happens after now
                }
                scheduleAlarm(i, schedules.get(i), c, am, now);
            }
        }
    }

    void scheduleAlarm(int index, Schedule s, Context c, AlarmManager am, Calendar now) {
        byte[] sb;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos;
        try {
            oos = new ObjectOutputStream(baos);
            oos.writeObject(s);
            sb = baos.toByteArray();
            oos.close();
        } catch (IOException e) {
            return;
        }
        scheduleAlarm(index, s, sb, c, am, now);
    }

    void scheduleAlarm(int index, Schedule s, byte[] sb, Context c, AlarmManager am, Calendar now) {
        if (now == null) {
            now = Calendar.getInstance();
        }
        Intent trigger = new Intent(c, SetRingerModeReceiver.class);
        trigger.putExtra("sched", sb);
        trigger.putExtra("index", index);
        PendingIntent ptrigger = PendingIntent.getBroadcast(c, index, trigger, PendingIntent.FLAG_ONE_SHOT | PendingIntent.FLAG_UPDATE_CURRENT);
        long windowStart = s.calculateNext(now);
        if (windowStart < 0) {
            return;
            // the alarm will not be scheduled again
            // so sad :(
        }
        if (exact) {
            if (Build.VERSION.SDK_INT < 23) {
                am.setExact(AlarmManager.RTC_WAKEUP, windowStart, ptrigger);
            }
            else {
                am.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, windowStart, ptrigger);
            }
        }
        else {
            if (s.ringerMode != AudioManager.RINGER_MODE_NORMAL) {
                windowStart -= window;
            }
            if (Build.VERSION.SDK_INT < 23) {
                am.set(AlarmManager.RTC_WAKEUP, windowStart, ptrigger);
            }
            else {
                am.setAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, windowStart, ptrigger);
            }
        }
        Log.d(TAG, "set alarm for " + windowStart);
    }
}
