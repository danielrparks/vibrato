package io.gitlab.danielrparks.vibrato;

import androidx.annotation.NonNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import ca.antonious.materialdaypicker.MaterialDayPicker;

public class WeekdaySelectionChecker implements Serializable {
    public static final long serialVersionUID = 1L;
    // for now, the Calendar class has the day of week constants built-in
    // as 1-7 for Sunday-Saturday
    private static int weekdayToCalendarDay(@NonNull MaterialDayPicker.Weekday weekday) {
        switch (weekday) {
            case SUNDAY:
                return Calendar.SUNDAY;
            case MONDAY:
                return Calendar.MONDAY;
            case TUESDAY:
                return Calendar.TUESDAY;
            case WEDNESDAY:
                return Calendar.WEDNESDAY;
            case THURSDAY:
                return Calendar.THURSDAY;
            case FRIDAY:
                return Calendar.FRIDAY;
            case SATURDAY:
                return Calendar.SATURDAY;
            default:
                throw new IllegalArgumentException("Weekday must be a Weekday.");
        }
    }

    private static MaterialDayPicker.Weekday calendarDayToWeekday(int calendarDay) {
        switch (calendarDay) {
            case Calendar.SUNDAY:
                return MaterialDayPicker.Weekday.SUNDAY;
            case Calendar.MONDAY:
                return MaterialDayPicker.Weekday.MONDAY;
            case Calendar.TUESDAY:
                return MaterialDayPicker.Weekday.TUESDAY;
            case Calendar.WEDNESDAY:
                return MaterialDayPicker.Weekday.WEDNESDAY;
            case Calendar.THURSDAY:
                return MaterialDayPicker.Weekday.THURSDAY;
            case Calendar.FRIDAY:
                return MaterialDayPicker.Weekday.FRIDAY;
            case Calendar.SATURDAY:
                return MaterialDayPicker.Weekday.SATURDAY;
            default:
                throw new IllegalArgumentException("Calendar day must be a Calendar day constant");
        }
    }

    private boolean[] days;

    void updateFromWeekdayList(List<MaterialDayPicker.Weekday> wds) {
        days = new boolean[7];
        for (MaterialDayPicker.Weekday i: wds) {
            days[weekdayToCalendarDay(i) - 1] = true;
        }
    }

    WeekdaySelectionChecker(boolean[] days) {
        if (days.length != 7) throw new IllegalArgumentException("A day boolean list must have 7 days");
        this.days = days;
    }

    List<MaterialDayPicker.Weekday> getWeekdayList() {
        ArrayList<MaterialDayPicker.Weekday> result = new ArrayList<>(7);
        for (int i = 0; i < days.length; i++) {
            if (days[i]) {
                result.add(calendarDayToWeekday(i+1));
            }
        }
        return result;
    }

    // will delete f I don't use it soon
    Set<Integer> getDaysFromCalendarList(Calendar now) {
        Set<Integer> result = new TreeSet<>();
        int nowdow = now.get(Calendar.DAY_OF_WEEK);
        for (int i = 1; i <= 7; i++) {
            if (days[i-1]) {
                if (i > nowdow) {
                    result.add(i - nowdow);
                }
                else if (i <= nowdow) {
                    result.add(i - nowdow + 7);
                }
            }
        }
        return result;
    }

    int getNextDay(Calendar now) {
        int nowdow = now.get(Calendar.DAY_OF_WEEK);
        for (int i = nowdow + 1; i != nowdow; i++) {
            if (i > 7) i = 1;
            if (days[i-1]) {
                if (i > nowdow) {
                    return  i - nowdow;
                }
                else {
                    return i - nowdow + 7;
                }
            }
        }
        return 7;
    }

    boolean hasSelection() {
        return
            days[0] ||
            days[1] ||
            days[2] ||
            days[3] ||
            days[4] ||
            days[5] ||
            days[6];
    }

    @NonNull
    public String toString() {
        return Arrays.toString(days);
    }
}
