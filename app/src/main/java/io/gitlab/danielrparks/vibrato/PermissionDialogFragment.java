package io.gitlab.danielrparks.vibrato;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.preference.PreferenceManager;

import java.util.Objects;

public class PermissionDialogFragment extends DialogFragment {
    private int permissionDescID;
    private int backupMessageID;
    private String prefKey;
    private Intent ifYes;

    PermissionDialogFragment(int permissionDescID, int backupMessageID, String prefKey, Intent ifYes) {
        this.permissionDescID = permissionDescID;
        this.backupMessageID = backupMessageID;
        this.prefKey = prefKey;
        this.ifYes = ifYes;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder b = new AlertDialog.Builder(Objects.requireNonNull(getActivity()));
        b.setMessage(permissionDescID)
                .setPositiveButton(R.string.ok, yes)
                .setNegativeButton(R.string.never, goAway)
                .setNeutralButton(R.string.procrastinate, null)
                .setCancelable(false);
        return b.create();
    }

    private void writePref() {
        SharedPreferences.Editor spe = PreferenceManager.getDefaultSharedPreferences(Objects.requireNonNull(getContext()).getApplicationContext()).edit();
        spe.putBoolean(prefKey, true);
        spe.apply();
    }

    private DialogInterface.OnClickListener goAway = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            writePref();
        }
    };

    private DialogInterface.OnClickListener yes = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            if (ifYes.resolveActivity(Objects.requireNonNull(getContext()).getApplicationContext().getPackageManager()) != null) {
                startActivity(ifYes);
            }
            else {
                Toast.makeText(getContext().getApplicationContext(), backupMessageID, Toast.LENGTH_LONG).show();
            }
        }
    };
}
