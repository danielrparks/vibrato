package io.gitlab.danielrparks.vibrato;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;


public class BootCompletedReceiver extends BroadcastReceiver {
    public static final String TAG = "BootCompletedReceiver";

    @Override
    public void onReceive(Context context, @NonNull Intent intent) {
        if (intent.getAction() == null || !intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            return; // spoofed intent
        }
        File schedData = new File(context.getFilesDir(), MainActivity.schedDataFilename);
        ArrayList<Schedule> schedules;
        synchronized (MainActivity.schedLock) {
            try {
                ObjectInputStream schedDataIn = new ObjectInputStream(new FileInputStream(schedData));
                //noinspection unchecked
                schedules = (ArrayList<Schedule>) schedDataIn.readObject();
                schedDataIn.close();
            } catch (FileNotFoundException e) {
                // literally don't do anything :)
                return;
            } catch (IOException e) {
                Log.e(TAG, "could not read schedule data, not scheduling!", e);
                return;
            } catch (ClassCastException | ClassNotFoundException e) {
                Log.e(TAG, "schedule data is corrupt, not scheduling!");
                return;
            }
        }
        // deleteLast set to true just in case the system crashed while deleting (unlikely)
        new UpdateAlarmsTask(context.getApplicationContext(), true).rescheduleAlarms(schedules);
    }
}
