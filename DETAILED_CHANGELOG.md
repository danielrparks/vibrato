1.1.0:

 - Add a setting that allows the user to choose how early Vibrato schedules Vibrate/Silent alarms (to compensate for Android deferring alarms)
 - Use synchronization to avoid race conditions and other small fixes
   - Minor performance improvement from not needing a file lock
   - Do not assume MainActivity will not be instantiated twice, obviously it will
   - Do not attempt to create the schedules file if it does not exist  
   This will result in an empty file and cause an error on next read
 - Fix a bug where new schedules would schedule alarms but not sync to disk (this is actually bad because it will schedule an Intent with an invalid index)
 - Accessibility improvements
 - Enable backup of app data (this was enabled by default, but now I'm saying what I mean)
 - Pass Schedules as serialized byte arrays through PendingIntents so that the alarm receiver doesn't need disk IO
 - Use AlarmManager.setAndAllowWhileIdle()
 - Create dialogs asking for permissions and allowing the user to postpone granting them
 - Ask for battery optimization whitelist so that alarms happen on time if the app is in a low-priority standby bucket
 - Enable ProGuard - the APK is now ~50% smaller!
